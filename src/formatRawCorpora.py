#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import tokeniser
import treetaggerwrapper
from nltk.tokenize import word_tokenize, sent_tokenize, wordpunct_tokenize
import subprocess
import codecs

def useTreeTagger(text, options):
	language_map={'it':'tree-tagger-italian-utf8','pt':'tree-tagger-portuguese'}
	#print "processing", text
	tt = subprocess.Popen(os.path.join(options.taggerPath, "cmd", language_map[options.language]), stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
	tags = tt.communicate(text)[0].split("\n")
	#print tt.returncode
	#print "processed", tags
	return tags


def prepStopWords(tagger, options):
        # process stop words, add all forms of stopwords!
        #stopWords1=[line.strip() for line in open("/home/siva/spandana/mel/RA_WORK/RA_SES/mallet/stoplists/en.txt").readlines()]
        stopWords=set(line.strip() for line in open(os.path.join(options.data,"seed_lexicon", options.language,"stopWords.txt")).readlines())
	#return stopWords
        from nltk import WordNetLemmatizer as wnl
	words= list(stopWords)
        for sword in words:
		if tagger!=None:
	                tags = tagger.TagText(sword)
		else:
			tags = useTreeTagger(sword, options)
                for tag in tags:
                        spl=tag.strip().split("\t")
                        if len(spl)==3:
                                if (spl[2]=="<unknown>" or spl[2]=="@card@" or spl[2]=="@ord@") and len(spl[2])>2:
                                        if spl[0] not in stopWords and "\\x" not in spl[0]:
                                                stopWords.add(spl[0])
                                elif len(spl[2])>2 and spl[2] not in stopWords:
					if "|" in spl[2]:
						data = spl[2].split("|")
						if data[0] not in words:
	                                        	words.append(data[0])
							stopWords.add(spl[2])
						if data[1] not in words:
	                                        	words.append(data[1])
							stopWords.add(data[1])
					else:
                                                stopWords.add(spl[2])
        #print stopWords
        return stopWords


def getContentText(comment, tagger, tTokeniser, stopWords, word_pos_data, options):

	#print text
	if tagger!=None:
	        tags = tagger.TagText(comment)
		#print comment, tags
	else:
		tags = useTreeTagger(comment, options)
        lemmaString=""
        words=set()
        for tag in tags:
                spl=tag.strip().split("\t")
                if len(spl)==3:
                        pos = spl[1].lower()[0]
                else:
                        pos='x'
                if pos in ['n', 'r', 'v', 'j', 'a']:
                        #spl[2] = correct(spl[2].lower())
                        if (spl[2]=="<unknown>" or spl[2]=="@card@" or spl[2]=="@ord@"):
                                spl[0] = spl[0].lower()
                                if "\\x" not in spl[0] and spl[0] not in stopWords and len(spl[0])>2 :
                                        lemmaString+=spl[0]+"#"+pos+" "
					if not word_pos_data['overall'].has_key(spl[0]+"#"+pos):
						word_pos_data['overall'][spl[0]+"#"+pos] = 0
					word_pos_data['overall'][spl[0]+"#"+pos] +=1
                        elif len(spl[2])>2 and spl[2] not in stopWords:
                                spl[2] = spl[2].lower()
				if "|" in spl[2]:
					data = spl[2].split("|")
				else:
					data = [spl[2]]
                                lemmaString+=data[0]+"#"+pos+" "
				if not word_pos_data['overall'].has_key(data[0]+"#"+pos):
					word_pos_data['overall'][data[0]+"#"+pos] = 0
				word_pos_data['overall'][data[0]+"#"+pos] +=1
        return lemmaString


def getSentences(document):
	sentences = sent_tokenize(document)
	for i in range(0, len(sentences)):
		tokenised = word_tokenize(sentences[i])		
		sentences[i] = " ".join(tokenised)
		#sentences[i] = "".join([x if ord(x) < 128 else '' for x in sentences[i]]).strip()
	#print sentences
	return sentences
	
def processFile(rawFile, dirname, tagger, options):
	if not os.path.exists(os.path.join(options.data,"tmp", options.language,dirname)):
		os.makedirs(os.path.join(options.data,"tmp",options.language, dirname))

	tTokeniser = tokeniser.MicroTokeniser()

	word_pos_data = {'overall':{}}

	fp = open(os.path.join(options.data,"tmp",options.language,dirname,"full_data.txt"), 'w')
	fp1 = open(os.path.join(options.data,"tmp",options.language,dirname,"clean_data.txt"), 'w')
	
	#for document in codecs.open(rawFile, 'r', 'utf-8'):
	for document in open(rawFile, 'r'):
		document = document.replace("\"","")
		#document = document.strip()[1:-1]
		document = document.decode("utf-8","replace")
		sentences = getSentences(document)
		#sentences = [" ".join(sentences)]	
		fp1.write(" ".join(sentences).encode("utf-8","replace")+"\n")
		for sentence in sentences:
			tagged_sentence = getContentText(sentence.encode("utf-8","replace"), tagger, tTokeniser, stopWords, word_pos_data, options)
			words = tagged_sentence.split()
			if len(words) >2:
				fp.write(tagged_sentence+"\n")	
	fp.close()
	fp1.close()

	import pickle
	pickle.dump(word_pos_data, open(os.path.join(options.data,"tmp",options.language,dirname,"word_pos_data.pckl"), 'w'))

if __name__ == "__main__":

	from optparse import OptionParser
	opts = OptionParser()
	opts.add_option("-d", "--data", dest="data", default="./data", help="data directory path, by default its set to data folder")
	opts.add_option("-t", "--taggerPath", dest="taggerPath", default="./TreeTagger", help="treetagger path")
	opts.add_option("-p", "--corporaPath", dest="corporaPath", default="../domain_corpora", help="given path to domain_corpora directory")
	opts.add_option("-c", "--category", dest="category", default="all", help="category/domain for which sentiment lexicon should be created")
	opts.add_option("-l", "--language", dest="language", default="en", help="Although same process could be applied all languages, seed lexicon we used is not available for french. If there is any reliable seed lexicon available for other languages please place it in data/seed_lexicon/fr")
	options, args = opts.parse_args()

	if options.language in ['en', 'de', 'fr', 'es', 'it', 'pt']:
		tagger = treetaggerwrapper.TreeTagger(TAGLANG=options.language,TAGDIR=options.taggerPath)
	else:
		tagger = None
	stopWords = prepStopWords(tagger, options)

	if options.category!="all":
		if not os.path.exists(os.path.join(options.corporaPath, options.language,options.category+".txt")):
			print "Please enter the category/domain name as it exists in domain_corpora folder (lowercase and spaces are replaced by '_')"
			sys.exit(0)
		print "processing", options.language, options.category
		processFile(os.path.join(options.corporaPath, options.language,options.category+".txt"), options.category, tagger, options)
	elif options.category=="all":
		for category in os.listdir(os.path.join(options.corporaPath, options.language)):
			print "processing", options.language, category.split(".txt")[0]
			processFile(os.path.join(options.corporaPath, options.language,category), category.split(".txt")[0], tagger, options)
					
