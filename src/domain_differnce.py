import  treetaggerwrapper
import os

def compareLexicon(lexicon1, lexicon2):
	intersection = 	set(lexicon1) & set(lexicon2)
	#print "intersection", intersection
	return len(intersection)


def analyseLexicon(category1, options, positive_seeds, negative_seeds):
	import csv 
	pos_lexicon1 = [line.strip().split("\t")[0] for line in open(os.path.join(options.lexiconPath,options.language,category1,"positive_lexicon.txt"), 'r')]
	neg_lexicon1 = [line.strip().split("\t")[0] for line in open(os.path.join(options.lexiconPath,options.language,category1,"negative_lexicon.txt"), 'r')]
	
	writer = csv.writer(open(os.path.join(options.lexiconPath, options.language, category1, "comparision_with_other_lexicons.csv"), 'w'))
	writer.writerow(["lexicon_name", "positive_lexicon_size", "positive_lexicon_intersection", "negative_lexicon_size", "negative_lexicon_intersection"])
	writer.writerow(["self", len(pos_lexicon1), len(pos_lexicon1), len(neg_lexicon1), len(neg_lexicon1)])
	
	#analysis with seed lexicon
	pos_inter_length = compareLexicon(pos_lexicon1, positive_seeds.keys())
	neg_inter_length = compareLexicon(neg_lexicon1, negative_seeds.keys())
	writer.writerow(["seed_lexicon", len(positive_seeds.keys()), pos_inter_length, len(negative_seeds.keys()), neg_inter_length])

	for category2 in os.listdir(os.path.join(options.lexiconPath, options.language)):
		if category1!=category2 and os.path.exists(os.path.join(options.lexiconPath,options.language,category2,"positive_lexicon.txt")):
			pos_lexicon2 = [line.strip().split("\t")[0] for line in open(os.path.join(options.lexiconPath,options.language,category2,"positive_lexicon.txt"), 'r')]
			neg_lexicon2 = [line.strip().split("\t")[0] for line in open(os.path.join(options.lexiconPath,options.language,category2,"negative_lexicon.txt"), 'r')]
			pos_inter_length = compareLexicon(pos_lexicon1, pos_lexicon2)
			neg_inter_length = compareLexicon(neg_lexicon1, neg_lexicon2)
			writer.writerow([category2 , len(pos_lexicon2), pos_inter_length, len(neg_lexicon2), neg_inter_length])

		

def getTagged(words, tagger, options):
        new_words = {}
        for word in words:
                #score = words[word]
                if tagger!=None:
                        tags = tagger.TagText(word)
                else:
                        tags = useTreeTagger(word, options)
                spl = tags[0].split("\t")
                pos = spl[1].lower()[0]
                if len(spl)==3 and pos in ['a','r','j','v','n']:
                        if (spl[2]=="<unknown>" or spl[2]=="@card@" or spl[2]=="@ord@"):
                                #new_words.add(spl[0].lower()+'#'+pos)
                                new_words[spl[0].lower()+'#'+pos] = 1
                        else:
                                #new_words.add(spl[2].lower()+'#'+pos)
                                new_words[spl[2].lower()+'#'+pos] = 1
        return new_words



def getSeeds(tagger, options):
        positive_words = set(line.strip().split("\t")[0].lower() for line in open(os.path.join(options.data,"seed_lexicon",options.language,"positive-words.txt"), 'r'))
        positive_words = getTagged(positive_words, tagger, options)
        negative_words = set(line.strip().split("\t")[0].lower() for line in open(os.path.join(options.data,"seed_lexicon",options.language,"negative-words.txt"), 'r'))
        negative_words = getTagged(negative_words, tagger, options)
        negative_markers = [line.strip() for line in open(os.path.join(options.data,"seed_lexicon", options.language,"negative_markers.txt"), 'r')]
        return positive_words, negative_words, negative_markers

if __name__ == "__main__":
	
	from optparse import OptionParser
        opts = OptionParser()
        opts.add_option("-d", "--data", dest="data", default="./data", help="data directory path, by default its set to data folder")
        opts.add_option("-p", "--lexiconPath", dest="lexiconPath", default="../domain_corpora_sentiment_lexicon", help="give path to domain_corpora_sentiment_lexicon directory")
        opts.add_option("-c", "--category", dest="category", default="all", help="category/domain for which sentiment lexicon should be analysed")
        opts.add_option("-l", "--language", dest="language", default="en", help="Select the language for which sentiment lexicon should be analysed, by default it will analyse english")
	opts.add_option("-t", "--taggerPath", dest="taggerPath", default="TreeTagger", help="treetagger path")

	options, args = opts.parse_args()

	if options.language in ['en', 'de', 'fr', 'es', 'it', 'pt']:
		tagger = treetaggerwrapper.TreeTagger(TAGLANG=options.language,TAGDIR=options.taggerPath)
	else:
		tagger = None

	positive_words, negative_words, negative_markers = getSeeds(tagger, options)

        if options.category!="all":
                if not os.path.exists(os.path.join(options.lexiconPath, options.language,options.category)):
                        print "Please enter the category/domain name as it exists in domain_corpora folder (lowercase and spaces are replaced by '_')"
                        sys.exit(0)
                print "processing", options.language, options.category
                analyseLexicon(options.category, options, positive_words, negative_words)
        elif options.category=="all":
                for category in os.listdir(os.path.join(options.lexiconPath, options.language)):
                        print "processing", options.language, category
                        analyseLexicon(category, options, positive_words, negative_words)
	

