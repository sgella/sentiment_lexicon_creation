#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
import operator
import pickle
import treetaggerwrapper
import os
import subprocess


def useTreeTagger(text, options):
        language_map={'it':'tree-tagger-italian-utf8','pt':'tree-tagger-portuguese'}
        tt = subprocess.Popen(os.path.join(options.taggerPath,"cmd", language_map[options.language]), stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        tags = tt.communicate(text)[0].split("\n")
        return tags


def getTagged(words, tagger, options):
	new_words = {}
	for word in words:
		#score = words[word]
		if tagger!=None:
			tags = tagger.TagText(word)
		else:
			tags = useTreeTagger(word, options)
		spl = tags[0].split("\t")
		pos = spl[1].lower()[0]
		if len(spl)==3 and pos in ['a','r','j','v','n']:
			if (spl[2]=="<unknown>" or spl[2]=="@card@" or spl[2]=="@ord@"):
				#new_words.add(spl[0].lower()+'#'+pos)
				new_words[spl[0].lower()+'#'+pos] = 1
			else:
				#new_words.add(spl[2].lower()+'#'+pos)
				new_words[spl[2].lower()+'#'+pos] = 1
	return new_words

def getSeeds(tagger, options):
	positive_words = set(line.strip().split("\t")[0].lower() for line in open(os.path.join(options.data,"seed_lexicon",options.language,"positive-words.txt"), 'r'))
	positive_words = getTagged(positive_words, tagger, options)
	negative_words = set(line.strip().split("\t")[0].lower() for line in open(os.path.join(options.data,"seed_lexicon",options.language,"negative-words.txt"), 'r'))
	negative_words = getTagged(negative_words, tagger, options)
	negative_markers = [line.strip() for line in open(os.path.join(options.data,"seed_lexicon", options.language,"negative_markers.txt"), 'r')]
	return positive_words, negative_words, negative_markers

def getWordCount(line, words):
	count = 0
	for word in words:
		if word in line:
			count+=1
	return count

def detectSentiment(line, pos_words, neg_words, neg_markers):
	neg_marker = 1
	sentiment = 0.0
	count =0
	neg_count = 0
	line = line.replace(".", "");
	total_count = 0
	for word in line.split():	
		#word = word.split('#')[0]
		if word in pos_words:
			sentiment +=neg_marker*pos_words[word]
			count+=1
		elif word in neg_words:
			sentiment +=neg_marker*-1*neg_words[word]
			count+=1
		if word[:-2] in neg_markers:
			neg_marker = -1
			neg_count +=1
		else:
			neg_marker = 1
		total_count+=1
	if total_count ==0 or count==0:
		return 0
	#print line, sentiment/count
	return sentiment/count

def readFullData(category, options):
	full_map = {}
	index = 0
	for line in open(os.path.join(options.data,"tmp",options.language,category,"full_data.txt"),'r'):
	#for line in open("../../data/bootstraping/full"+type.title()+"/full_data.txt"):
		full_map[str(index)] = line
		index+=1
	return full_map

def do_iteration(positive_words, negative_words, full_data, min_count, neg_markers, covered_indexes):
	pos_map = {}
	neg_map = {}
	sentiment_map = {}
	index = 0
	pos_count = 0 
	neg_count = 0
	for index in full_data:	
		if index not in covered_indexes['+'] and index not in covered_indexes['-'] and len(full_data[index].split())<150:
			sentiment = detectSentiment(full_data[index], positive_words, negative_words, neg_markers)
			if sentiment!=0:
				sentiment_map[index] = sentiment
			if sentiment >0:
				pos_count+=1
			if sentiment<0:
				neg_count+=1
	return sentiment_map

def generateNewLexicon(sorted_map, cur_positive_words, cur_negative_words, negative_markers, covered_indexes, polarity_words_counts, data_counts, min_word_count, options, i):
	exclude_list = set()
	exclude_list.update(negative_markers)

	length = len(sorted_map)
	pos_words = {}
	neg_words = {}
	
	if length ==0:
		return pos_words, neg_words
	if len(sorted_map)>1:
		min_score = sorted_map[len(sorted_map)-1][1]
	else:
		min_score = sorted_map[0][1]
	max_score = sorted_map[0][1]
	med_score = sorted_map[len(sorted_map)/2][1]

	#min_score = sorted_map[length/10][1]
	#max_score = sorted_map[(9*length)/10][1]

	#print "threshold", min_score,max_score,med_score,max(0,(med_score+min_score)/2),max((med_score+max_score)/2, 0.1)
	#print "threshold", min_score,max_score,med_score,max(0,(med_score+min_score)/2),max((med_score+max_score)/2, 0.1)

	if options.verbose == "True":
		print "Min, median and max sentiment scores in iteration i", i, min_score, med_score, max_score
	
	count = 0
	for index, score in sorted_map:
 		count+=1
		if (index not in covered_indexes['+'] and index not in covered_indexes['-']):
			for word in full_data[index].split():
				if len(word) >4 and data_counts['overall'][word]>min_word_count and word[:-2] not in exclude_list and word[:-2].isalpha() :
					#word = word[:-2]
					if count>=float(len(sorted_map)*2)/3.0 and score<0:
					#if score<=max(0,(med_score+min_score)/2):
						covered_indexes['-'].add(index)
						#neg
						if not polarity_words_counts.has_key(word):
							polarity_words_counts[word] = [0.0,0.0]
						polarity_words_counts[word][1]+=1
					#elif score>= max((med_score+max_score)/2, 0.1):
					if count<=float(len(sorted_map))/3.0 and score>0.1:
						covered_indexes['+'].add(index)
						#pos
						if not polarity_words_counts.has_key(word):
							polarity_words_counts[word] = [0.0,0.0]
						polarity_words_counts[word][0]+=1	

	#print "positive and negative reviews", len(covered_indexes['+']), len(covered_indexes['-'])
	for word in polarity_words_counts:		
		if sum(polarity_words_counts[word])>1:# and data_counts['overall'][word]>100 and word not in exclude_list:
			#word = word[:-2]
			ppos = polarity_words_counts[word][0]/sum(polarity_words_counts[word])
			pneg = polarity_words_counts[word][1]/sum(polarity_words_counts[word])
			if ppos > pneg :
					pos_words[word] = ppos
				#print "Adding positive", word
			elif pneg>0:
				neg_words[word] = pneg
				#print "Adding negative", word
	#print pos_words, neg_words
	return pos_words, neg_words

def filterWords(pos_words):
	new_list = set()
	for word in pos_words:
		if not word.isalnum():
			new_list.add(word)
	return new_list
	

if __name__ == "__main__":

	from optparse import OptionParser
        opts = OptionParser()
        opts.add_option("-d", "--data", dest="data", default="data", help="data directory path, by default its set to data folder")
        opts.add_option("-t", "--taggerPath", dest="taggerPath", default="TreeTagger", help="treetagger path")
        opts.add_option("-v", "--verbose", dest="verbose", default="False", help="print extra details")
        opts.add_option("-c", "--category", dest="category", default="all", help="category/domain for which sentiment lexicon should be created")	
        opts.add_option("-o", "--output_dir", dest="output_dir", default="../domain_corpora_sentiment_lexicon", help="output directory for writing created sentiment specific lexicon")
        opts.add_option("-m", "--min_word_count", dest="min_word_count", type = "int", default="25", help="By default minimum word count required to filter the word into sentiment lexicon is set to 25")
        opts.add_option("-i", "--iterations", dest="iterations", type = "int", default="10", help="Boot strapping algorithm is run iteratively, this options should be chosen cleverly based on the dataset. By default we set it to 10 which seems to work in most cases.")
        opts.add_option("-l", "--language", dest="language", default="en", help="Although same process could be applied to other languages, seed lexicon we used is just available for english. If there is any reliable seed lexicon available make sure you format your corpora (look README) before running this script.")
        options, args = opts.parse_args()

	#if not os.path.exists(os.path.join(options.output_dir,options.language)):
	#	os.makedirs(os.path.join(options.output_dir,options.language))
	
	if options.language in ['en','fr','es','de','it','pt']:
		tagger = treetaggerwrapper.TreeTagger(TAGLANG=options.language,TAGDIR=options.taggerPath)	
	else:
		tagger = None

	
	categories= []

	if options.category=="all":
		for category in os.listdir(os.path.join(options.data,"tmp",options.language)):
			categories.append(category)
	elif os.path.exists(os.path.join(options.data,"tmp",options.language,options.category)):
		categories.append(options.category)
	else:
		print "please check if the entered category is correct dewey category"
		sys.exit(0)

	for category in categories:
		positive_seeds, negative_seeds, negative_markers = getSeeds(tagger, options)
		print "working on category", category, len(positive_seeds), len(negative_seeds)
		full_data = readFullData(category, options)

		if not os.path.exists(os.path.join(options.output_dir,options.language,category)):
			os.makedirs(os.path.join(options.output_dir,options.language,category))

		pos_words = positive_seeds
		neg_words = negative_seeds

		#print pos_words
		i=1
		covered_indexes={'+':set(), '-':set()}
		polarity_corpus_counts={}
		if os.path.exists(os.path.join(options.data,"tmp",options.language,category,"word_pos_data.pckl")):
			data_counts = pickle.load(open(os.path.join(options.data,"tmp",options.language,category,"word_pos_data.pckl"), 'r'))
			length = len(data_counts['overall'])
		else:
			print "This category doesn't have pre-processed files, run formatRawCorpora for the category", options.language, category
		#print length
		sorted_data = sorted(data_counts['overall'].items(), key=operator.itemgetter(1), reverse = True)
		if length>0:
			item , min_word_count = sorted_data[length/20]
			if options.min_word_count!=25:
				min_word_count = options.min_word_count
			else:
				min_word_count = max(options.min_word_count, min_word_count)
			print "Auto min count set for word filtering", min_word_count, length
		while i<options.iterations and length>0:
			#print "Iteration", i
			#pos_words = filterWords(pos_words)
			#neg_words = filterWords(neg_words)
			#print list(pos_words)[:5], list(neg_words)[:5]
			sentiment_map = do_iteration(pos_words, neg_words, full_data, i, negative_markers, covered_indexes)
			sorted_map = sorted(sentiment_map.items(), key=operator.itemgetter(1), reverse = True)
			if len(sorted_map)>0:
				pos_words, neg_words = generateNewLexicon(sorted_map, pos_words, neg_words, negative_markers, covered_indexes, polarity_corpus_counts, data_counts, min_word_count, options, i)
				#print "length of positive, negative", len(pos_words), len(neg_words)
			i = i+1

		pos_count = 0
		if length>0:
			#os.path.join(options.output_dir,options.language,category,"positive_lexicon.txt")
			fp= open(os.path.join(options.output_dir,options.language,category,"positive_lexicon.txt"),'w')
			for word,prob in sorted(pos_words.items(), key=operator.itemgetter(1), reverse = True):
				fp.write(word+"\t"+str(prob)+"\t"+str(data_counts['overall'][word])+"\n")
				if word not in positive_seeds:
					pos_count +=1
			fp.close()

			neg_count = 0
			fp= open(os.path.join(options.output_dir,options.language,category,"negative_lexicon.txt"),'w')
			for word,prob in sorted(neg_words.items(), key=operator.itemgetter(1), reverse = True):
				fp.write(word+"\t"+str(prob)+"\t"+str(data_counts['overall'][word])+"\n")
				if word not in negative_seeds:
					neg_count +=1
			fp.close()
			print "Final lengths", len(pos_words), len(neg_words), "Newly added positive and negative", pos_count, neg_count
