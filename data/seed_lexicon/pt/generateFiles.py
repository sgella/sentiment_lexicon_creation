import sys

fp = open("positive-words.txt", 'w')
fn = open("negative-words.txt", 'w')

for line in open("./SentiLex-lem-PT01.txt", 'r'):
	spl = line.strip().split(";")
	word = spl[0].split(".PoS")[0]
	pol = spl[3].split("POL=")[1]
	if int(pol) == -1:
		fn.write(word+"\n")
	if int(pol) == 1:
		fp.write(word+"\n")

fp.close()
fn.close()
