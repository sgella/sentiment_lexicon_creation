import sys

fp = open("positive-words.txt", 'w')
fn = open("negative-words.txt", 'w')

for line in open("mediumStrengthLexicon.txt", 'r'):
	spl = line.strip().split()
	if len(spl) == 3 or spl[2]== spl[3]:
		if spl[2] == "pos":
			fp.write(spl[0]+"\n")
		if spl[2] == "neg":
			fn.write(spl[0]+"\n")
fp.close()
fn.close()
